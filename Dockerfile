FROM php:8.2.10-alpine

RUN apk update && \
    apk upgrade && \
    apk add --update linux-headers

# Install git, wget, zip, PHP Deps, Xdebug
RUN apk add npm git wget zip unzip libzip-dev $PHPIZE_DEPS && \
    pecl install xdebug

# Install Local PHP security checker
RUN curl -L https://github.com/fabpot/local-php-security-checker/releases/download/v2.0.6/local-php-security-checker_2.0.6_linux_amd64 --output /usr/local/bin/local-php-security-checker && \
    chmod +x /usr/local/bin/local-php-security-checker

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

# Enable PHP extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql zip && \
    docker-php-ext-enable xdebug
